﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Genetic.Algorithm;

namespace Genetic.Algorithm
{
    public class Specimen
    {
        public int Value { get; set; }
        public BitArray BinaryValue { get; set; }
        public double Fitness { get; set; }


        public Specimen (int value)
        {
            Value = value;
            BinaryValue = new BitArray(new int[] { value });
            Fitness = 0;
        }

        public Specimen(BitArray binaryValue)
        {
            Value = binaryValue.GetIntFromBitArray();
            BinaryValue = binaryValue;
            Fitness = 0;
        }
        
        public Specimen(BitArray binaryValue, double fitness)
        {
            Value = binaryValue.GetIntFromBitArray();
            BinaryValue = binaryValue;
            Fitness = fitness;
        }


        // Get Specimen string operator

        public static String GetString(Specimen spec, int range)
        {
            String result = spec.Value.ToString() + " dec [";

            for (int i = 1; i < range+1; i++)
            {
                // Cast b to zero or one through boolean
                //result += spec.BinaryValue[i] is bool idk && idk ? 1 : 0;
                
                result += (bool)spec.BinaryValue[i] ? 1 : 0;
            }

            result += "] bin F:" + spec.Fitness;

            return result;
        }

        // Create array of random specimen
        public static Specimen[] MakeRandomSpecimenArray(int seed, int amount, int range)
        {
            Specimen[] specimenArr = new Specimen[amount];
            int max = 0; // Convert.ToInt32(Math.Pow(range, 2));

            for (int i = 0; i < range; i++)
            {
                max += Convert.ToInt16(Math.Pow(2, i));
            }

            // Create seeded Random
            Random rnd = new Random(seed);

            for (int i = 0; i < specimenArr.Length; i++)
            {
                // Generate seeded random integer in range
                int value = rnd.Next(max);

                specimenArr[i] = new Specimen(value);
            }

            return specimenArr;
        }

        // Crossing operator
        public static Tuple<Specimen, Specimen> Cross (Specimen spec1, Specimen spec2, int position,
                                                      int a, int b, int c, int d)
        {
            BitArray bv1 = spec1.BinaryValue;
            BitArray bv2 = spec2.BinaryValue;

            BitArray bv1new = new BitArray(bv1.Length);
            BitArray bv2new = new BitArray(bv2.Length);

            Specimen spec1new;
            Specimen spec2new;

            for (int i = 0; i < position; i++)
            {
                bv1new[i] = bv1[i];
                bv2new[i] = bv2[i];
            }

            for (int i = position; i < bv1.Length; i++)
            {
                bv1new[i] = bv2[i];
                bv2new[i] = bv1[i];
            }
            
            spec1new = new Specimen(bv1new);
            spec2new = new Specimen(bv2new);
            
            spec1new.Fitness = CalculateFitness(spec1new,a,b,c,d);
            spec2new.Fitness = CalculateFitness(spec2new,a,b,c,d);

            Tuple<Specimen, Specimen> result = new Tuple<Specimen, Specimen>(spec1new, spec2new);

            return result;
        }

        // Mutation operator
        public static Specimen Mutate (Specimen specimen, int position)
        {
            BitArray tmp = new BitArray(specimen.BinaryValue);
            tmp[position] = !tmp[position];

            return new Specimen(tmp);
        }

        // Fitness
        public static double CalculateFitness(Specimen spec, int aParam, int bParam, int cParam, int dParam)
        {
            double x = spec.Value;

            return (Math.Sin(x + aParam) + x / bParam) / cParam * Math.Log10(dParam); //aParam * Math.Cos(x) - ((bParam * (Math.Sin(x)*Math.Sin(x))) / Math.Sqrt(cParam)) * Math.Log(dParam) + x;
        }

        // Roulette
        public static int[] Roulette(Specimen[] specArr, int seed, int range)
        {
            int max = Convert.ToInt32(Math.Pow(range, 2));

            double[] roulette = new double[specArr.Length + 1];
            int[] survivingIndexes = new int[specArr.Length];
            int lastIndex = roulette.Length - 1;
            
            var minVal = specArr.OrderByDescending(i => i.Fitness).LastOrDefault();
            
            if (minVal.Fitness < 0)
            {
	            foreach (Specimen s in specArr)
	            {
	            	s.Fitness += Math.Abs(minVal.Fitness * 2);
	            }
            }

            roulette[0] = 0;

            for (int i = 1; i < roulette.Length; i++)
            {
                roulette[i] = roulette[i-1] + specArr[i-1].Fitness;
            }
            

            // Create seeded Random
            Random rnd = new Random(seed);

            for (int i = 0; i < survivingIndexes.Length; i++)
            {
                double rand = rnd.NextDouble() * roulette[lastIndex];

                for (int j = 0; j < roulette.Length - 1; j++)
                {
                    if (rand >= roulette[j] && rand < roulette[j+1])
                    {
                        survivingIndexes[i] = j;
                        //break;
                    }

                }
            }

            return survivingIndexes;
        }

    }
}
