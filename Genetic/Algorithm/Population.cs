﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Genetic.Algorithm;

namespace Genetic.Algorithm
{

	public static class Population
	{
		
		// New rouletted population
        public static Specimen[] NewPopulationRoulette(Specimen[] population, int[] roulette)
        {
            Specimen[] newPop = new Specimen[population.Length];

            for (int i = 0; i < population.Length; i++)
            {
                newPop[i] = population[roulette[i]];
            }

            return newPop;
        }

        
        // Method performing crossing operation on population
        public static Specimen[] CrossPopulation(Specimen[] population, int crossingChance, int seed, int range,
                                                int a, int b, int c, int d)
        {
        	Random rnd = new Random(seed);
        	Specimen[] result = new Specimen[population.Length];
        	
            for (int i = 0; i < population.Length; i += 2)
            {
            	int chance = rnd.Next(100);
            	int position = rnd.Next(range - 1);
            	
            	// Check if it's a lonely specimen
            	if(i+1 < population.Length)
            	{
            		// If not, see if the pair crosses
	            	if (chance <= crossingChance)
	            	{
		            	Tuple<Specimen, Specimen> tmpSpecTuple =  Specimen.Cross(population[i], population[i+1], position,
	            		                                                        a,b,c,d);
		            	
		            	result[i] = tmpSpecTuple.Item1;
		            	result[i+1] = tmpSpecTuple.Item2;
	            	}
	            	else
	            	{
	            		result[i] = population[i];
		            	result[i+1] = population[i+1];
	            	}
	            	
            	}
            	else
            	{
            		result[i] = population[i];
            	}
            	
            }
            
            return result;
        }
        
        // Mutate population
        public static Specimen[] MutatePopulation(Specimen[] population, int mutationChance, int seed, int range,
                                                  int a, int b, int c, int d)
        {       
	        Random rnd = new Random(seed);
        	Specimen[] result = new Specimen[population.Length];

            for (int i = 0; i < population.Length; i++)
            {
                int chance = rnd.Next(100);
                int position = rnd.Next(range);

                if (chance <= mutationChance)
                { 
                    result[i] = Specimen.Mutate(population[i], position);
            	}
            	else
            	{
                    result[i] = population[i];
            	}

            }

            return result;
        }

        // Recalculate population
        public static Specimen[] RecalculatePopulation(Specimen[] population, int aParam, int bParam, int cParam, int dParam)
        {
            Specimen[] tmpPop = population;

            foreach (Specimen spec in tmpPop)
            {
                spec.Value = Operations.GetIntFromBitArray(spec.BinaryValue);
                spec.Fitness = Specimen.CalculateFitness(spec, aParam, bParam, cParam, dParam);
            }

            return tmpPop;
        }
	}
}
