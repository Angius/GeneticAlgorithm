﻿/*
 * Created by SharpDevelop.
 * User: student
 * Date: 13.11.2017
 * Time: 12:46
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using Genetic.Algorithm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.ComponentModel;

namespace Genetic
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		int seed;
		int population;
		int crossingChance;
		int mutationChance;
		double satisfactoryResult;

        int range = 8;

        int a, b, c, d;

        bool toggleDisplay = true;

		// Main window
		public Window1()
		{
			InitializeComponent();

            ConsoleOutput_Textbox.Text = @"Calculating:
    Pow(x, aParam) * (Log10(bParam) * x) + Sin(Sqrt(cParam) * dParam)" + Environment.NewLine + Environment.NewLine;
		}

        // HANDLE TITLE BAR

        // drag
        private void Titlebar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        // Buttons
        private void CloseWindow_Button_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }


        bool windowStateFlipFlop = false;
        private void MaximizeWindow_Button_Click(object sender, RoutedEventArgs e)
        {
            if (windowStateFlipFlop)
            {
                WindowState = WindowState.Normal;
                windowStateFlipFlop = false;
            } else {
                WindowState = WindowState.Maximized;
                windowStateFlipFlop = true;
            }
        }

        private void MinimizeWindow_Button_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void ScrollToTop_Button_Click(object sender, RoutedEventArgs e)
        {
            Console_ScrollViewer.ScrollToTop();
        }

        private void ScrollToBottom_Button_Click(object sender, RoutedEventArgs e)
        {
            Console_ScrollViewer.ScrollToBottom();
        }


        // HANDLE BUTTONS

        // Randomize Seed button
        void RandomizeSeed_Button_Click(object sender, RoutedEventArgs e)
		{
			Random rnd = new Random();
			Seed_TextBox.Text = rnd.Next().ToString();
		}

        private void ToggleDisplay_Button_Click(object sender, RoutedEventArgs e)
        {
            if (toggleDisplay)
            {
                toggleDisplay = false;
                ToggleDisplay_Button.Content = "Disabled";
            }
            else
            {
                toggleDisplay = true;
                ToggleDisplay_Button.Content = "Enabled";
            }
        }

        // Run algorithm button
        void Run_Button_Click(object sender, RoutedEventArgs e)
		{
			// Set BackgroundWorker
			//BackgroundWorker worker = sender as BackgroundWorker;
			
            // Set variables
			population = Int32.Parse(Population_TextBox.Text);
            range = Convert.ToInt32(Range_Slider.Value);
			crossingChance = Convert.ToInt32(CrossingChance_Slider.Value);
			mutationChance = Convert.ToInt32(MutationChance_Slider.Value);
			seed = Int32.Parse(Seed_TextBox.Text);

            satisfactoryResult = SatisfactoryResult_Slider.Value;// / 100;

            a = Int32.Parse(A_Param_Textbox.Text);
            b = Int32.Parse(B_Param_Textbox.Text);
            c = Int32.Parse(C_Param_Textbox.Text);
            d = Int32.Parse(D_Param_Textbox.Text);


            Specimen[] specArr = Specimen.MakeRandomSpecimenArray(seed, population, range);

            for (int cnt = 0; cnt < satisfactoryResult; cnt++)
            {
                // Create an array of fitnesses
                for (int i = 0; i < specArr.Length; i++)
                {
                    specArr[i].Fitness = Specimen.CalculateFitness(specArr[i], a, b, c, d);
                }

                if (toggleDisplay)
                {
                    // Output population to console
                    for (int i = 0; i < specArr.Length; i++)
                    {
                        ConsoleOutput_Textbox.Text += "[s" + i + "]: " + Specimen.GetString(specArr[i], range) + Environment.NewLine;
                    }
                }

                // Spin the roulette...
                int[] rou = Specimen.Roulette(specArr, seed, range);
                // ...and may the strongest survive!
                specArr = Population.NewPopulationRoulette(specArr, rou);
                // Cross the population
                specArr = Population.CrossPopulation(specArr, crossingChance, seed, range, a, b, c, d);
                // And mutate it
                specArr = Population.MutatePopulation(specArr, mutationChance, seed, range, a, b, c, d);
                // Recalculate just in case
                //specArr = Population.RecalculatePopulation(specArr, a, b, c, d);

                //worker.ReportProgress(Convert.ToInt32((100 * cnt) / satisfactoryResult));
                
                ConsoleOutput_Textbox.Text += "\n\n";
            }
			
            // Get maximum fitness
            double maxFitness = 0.0;
            int maxFitnessI = 0;
            
            for (int i = 0; i < specArr.Length; i++)
            {
            	if (maxFitness < specArr[i].Fitness)
            	{
            		maxFitness = specArr[i].Fitness;
            		maxFitnessI = i;
            	}
            }
            
            Specimen topSpecimen = specArr[maxFitnessI];
            
            /*
             BinaryValue in 105 is array of 0s
             Therefore fitness is 0
             But somehow value is 105
             */
            
            //var maxVal = specArr.Max(x => x.Fitness); // get highest fitness
            //var topSpecimen = specArr.First(x => x.Fitness == maxVal);

            // Print the best specimen
            ResultOutput_Textbox.Text = "The top specimen is: " + Specimen.GetString(topSpecimen, range) + "\nAnd thus, the solution is: " + topSpecimen.Value;

            var tmpSA = specArr;
		}
		
		// Reset console button
		void Reset_Button_Click(object sender, RoutedEventArgs e)
		{
			ConsoleOutput_Textbox.Text = "";
			ResultOutput_Textbox.Text = "";
		}
		
		// Worker
		private void Worker_ProgressChange (object sender, ProgressChangedEventArgs e)
		{
			//computationProgress.Value = Math.Min(e.ProgressPercentage, 100);
		}

    }
}